/*global QUnit*/

jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 AbsenceEntriesSet in the list

sap.ui.require([
	"sap/ui/test/Opa5",
	"Z03_/AbsenseApproval/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"Z03_/AbsenseApproval/test/integration/pages/App",
	"Z03_/AbsenseApproval/test/integration/pages/Browser",
	"Z03_/AbsenseApproval/test/integration/pages/Master",
	"Z03_/AbsenseApproval/test/integration/pages/Detail",
	"Z03_/AbsenseApproval/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "Z03_.AbsenseApproval.view."
	});

	sap.ui.require([
		"Z03_/AbsenseApproval/test/integration/MasterJourney",
		"Z03_/AbsenseApproval/test/integration/NavigationJourney",
		"Z03_/AbsenseApproval/test/integration/NotFoundJourney",
		"Z03_/AbsenseApproval/test/integration/BusyJourney",
		"Z03_/AbsenseApproval/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});