/*global QUnit*/

jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"Z03_/AbsenseApproval/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"Z03_/AbsenseApproval/test/integration/pages/App",
	"Z03_/AbsenseApproval/test/integration/pages/Browser",
	"Z03_/AbsenseApproval/test/integration/pages/Master",
	"Z03_/AbsenseApproval/test/integration/pages/Detail",
	"Z03_/AbsenseApproval/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "Z03_.AbsenseApproval.view."
	});

	sap.ui.require([
		"Z03_/AbsenseApproval/test/integration/NavigationJourneyPhone",
		"Z03_/AbsenseApproval/test/integration/NotFoundJourneyPhone",
		"Z03_/AbsenseApproval/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});